(function (window, Drupal, $, lazyLoader) {
  Drupal.Ajax.prototype.trueBeforeSend = Drupal.Ajax.prototype.beforeSend;
  Drupal.Ajax.prototype.beforeSend = function (xmlhttprequest, options) {
    // Add headers to request.
    if (this.hasOwnProperty('placeholder')) {
      xmlhttprequest.setRequestHeader('placeholder', this.placeholder)
    }
    Drupal.Ajax.prototype.trueBeforeSend.call(this, xmlhttprequest, options)
  };
  lazyLoader.bind(window);
}(window, Drupal, jQuery, {
  bind: function (window) {
    window.addEventListener('lazybeforeunveil', function (e) {
      if (e.target.classList.contains('lazy_render')) {
        const placeholder = e.target.getAttribute('data-ajax-placeholder');
        // Skip if placeholder not exist.
        if (!placeholder) {
          return;
        }
        if(e.defaultPrevented) {
          return;
        }
        const ajax = Drupal.ajax({
          url: location.href,
          progress: false,
          beforeSend: function (xhr, options) {
            if (options.url) {
              options.url = options.url.replace('_wrapper_format=drupal_ajax', '')
            }
            xhr.setRequestHeader('placeholder', placeholder)
          }
        })
        ajax.success = function (response, status) {
          for (let i in response || {}) {
            if (!response.hasOwnProperty(i)) {
              continue;
            }
            if (response[i].command === 'settings') {
              for (let key in response[i].settings.ajax) {
                if (!response[i].settings.ajax.hasOwnProperty(key)) {
                  continue;
                }
                response[i].settings.ajax[key].placeholder = placeholder;
              }
            }
            if (response[i].command && this.commands[response[i].command]) {
              if (response[i].selector === 'lazy_render') {
                // Set selector by our element.
                response[i].selector = e.target;
              }
              this.commands[response[i].command](this, response[i], status);
            }
          }
        }
        ajax.execute();
      }
    })
  }
}));
