<?php

namespace Drupal\lazy_render\Render\Placeholder;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Crypt;
use Drupal\Component\Utility\Html;
use Drupal\Core\Render\Placeholder\PlaceholderStrategyInterface;
use Drupal\Core\Site\Settings;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides placeholder strategy.
 */
class LazyRenderStrategy implements PlaceholderStrategyInterface {

  /**
   * BigPipe no-JS cookie name.
   */
  const NOJS_COOKIE = 'lazy_render_nojs';

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * LazyRenderStrategy constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   */
  public function __construct(RequestStack $requestStack) {
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public function processPlaceholders(array $placeholders) {
    if ($this->requestStack->getCurrentRequest()->cookies->has(static::NOJS_COOKIE)) {
      return $placeholders;
    }
    foreach ($placeholders as $placeholder => $placeholder_render_array) {
      if (stripos($placeholder_render_array['#lazy_builder'][0], 'lazy_render') !== FALSE) {
        if (!$this->placeholderIsAttributeSafe($placeholder)) {
          $placeholders[$placeholder] = $this->buildPlaceholder($placeholder_render_array);
        }
      }
    }
    return $placeholders;
  }

  /**
   * Build placeholder element.
   *
   * @param array $placeholder_data
   *   The source plazeholder data.
   *
   * @return array
   *   The placeholder render element.
   */
  protected function buildPlaceholder(array $placeholder_data) {
    $callback = $placeholder_data['#lazy_builder'][0];
    $args = $placeholder_data['#lazy_builder'][1];

    return [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#cache' => [
        'max-age' => 0,
      ],
      '#attributes' => [
        'class' => 'lazy_render lazyload',
        'data-ajax-placeholder' => Json::encode([
          'callback' => $placeholder_data['#lazy_builder'][0],
          'args' => $placeholder_data['#lazy_builder'][1],
          'token' => self::generateToken($callback, $args),
        ]),
      ],
      '#attached' => [
        'library' => ['lazy_render/loader'],
      ],
    ];
  }

  /**
   * Determines whether the given placeholder is attribute-safe or not.
   *
   * @param string $placeholder
   *   A placeholder.
   *
   * @return bool
   *   Whether the placeholder is safe for use in a HTML attribute (in case it's
   *   a placeholder for a HTML attribute value or a subset of it).
   *
   * @see \Drupal\big_pipe\Render\Placeholder\BigPipeStrategy::placeholderIsAttributeSafe
   */
  protected static function placeholderIsAttributeSafe($placeholder) {
    assert(is_string($placeholder));
    return $placeholder[0] !== '<' || $placeholder !== Html::normalize($placeholder);
  }

  /**
   * Generates token for protection from random code executions.
   *
   * @param string $callback
   *   The callback function.
   * @param array $args
   *   The callback arguments.
   *
   * @return string
   *   The token that sustain across requests.
   */
  public static function generateToken(string $callback, array $args): string {
    // Use hash salt to protect token against attacks.
    $token_parts = [$callback, $args, Settings::get('hash_salt')];
    return Crypt::hashBase64(serialize($token_parts));
  }

}
