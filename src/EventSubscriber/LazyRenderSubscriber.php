<?php

namespace Drupal\lazy_render\EventSubscriber;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Render\RendererInterface;
use Drupal\lazy_render\Render\Placeholder\LazyRenderStrategy;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Lazy strategy event subscriber.
 */
class LazyRenderSubscriber implements EventSubscriberInterface {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(RendererInterface $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * Kernel view event handler.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   Response event.
   */
  public function onLazyRenderView(GetResponseEvent $event) {
    $decode = Json::decode($event->getRequest()->headers->get('placeholder'));
    $skip = FALSE;
    foreach (['callback', 'token', 'args'] as $param) {
      if (!isset($decode[$param])) {
        $skip = TRUE;
      }
    }
    if ($skip) {
      return;
    }
    $callback = $decode['callback'];
    $args = isset($decode['args']) ? $decode['args'] : [];
    $token = $decode['token'];
    if (LazyRenderStrategy::generateToken($callback, $args) !== $token) {
      return;
    }
    $render_array = [
      '#lazy_builder' => [$callback, $args],
      '#create_placeholder' => FALSE,
    ];

    $response = new AjaxResponse();
    $content = $this->renderer->renderRoot($render_array);
    $response->setAttachments($render_array['#attached']);
    $response->addCommand(new ReplaceCommand('lazy_render', $content));
    $event->setResponse($response);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [KernelEvents::VIEW => ['onLazyRenderView', 500]];
  }

}
