<?php

namespace Drupal\lazy_render\Controller;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Routing\LocalRedirectResponse;
use Drupal\lazy_render\Render\Placeholder\LazyRenderStrategy;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Returns responses for Lazy strategy routes.
 */
class LazyRenderController implements ContainerInjectionInterface {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Sets no-JS cookie, redirects back to the original location.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   */
  public function noJsCookie(Request $request) {

    if ($request->cookies->has(LazyRenderStrategy::NOJS_COOKIE) || !$request->hasSession()) {
      throw new AccessDeniedHttpException();
    }

    if (!$request->query->has('destination')) {
      throw new HttpException(400, 'The original location is missing.');
    }

    $response = new LocalRedirectResponse($request->query->get('destination'));
    // Set cookie without httpOnly, so that JavaScript can delete it.
    $response
      ->headers
      ->setCookie(new Cookie(
        LazyRenderStrategy::NOJS_COOKIE,
        TRUE,
        0,
        '/',
        NULL,
        FALSE,
        FALSE,
        FALSE,
        NULL
      ));
    $response->addCacheableDependency((new CacheableMetadata())->addCacheContexts(['cookies:' . LazyRenderStrategy::NOJS_COOKIE]));
    return $response;
  }

}
