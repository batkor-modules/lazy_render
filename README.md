## Lazy render
This module provides better performance you Drupal site.

### How this worked?
Provides new better placeholder strategy.\
At first checking js support in browser. (meta#noscript);\
1. Browser support js;
    - Instead real HTML content render placeholder element;
    - Detects placeholder visibility
    - Rendering real HTML content
2. Browser not support js.
    - Render real HTML content.

### Feature
1. Support #ajax elements in Forms;
2. For render real HTML content used current URL.
3. Working only if service name contains `lazy_render`. (`you_module.lazy_render`) (Need test for every variations)

### Dependency
This module need [lazysizes](https://github.com/aFarkas/lazysizes) library.
